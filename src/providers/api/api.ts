import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

const apiUrl = "https://jsonplaceholder.typicode.com";

@Injectable()
export class ApiProvider {

  public URL = 'https://jsonplaceholder.typicode.com/';

  constructor(public http: HttpClient) {
    console.log('Hello ApiProvider Provider');
  }

  private handleError(error: HttpErrorResponse) {
  	if (error.error instanceof ErrorEvent) {
  		console.error('An error occurred:', error.error.message);
  	} else {
  		console.error(
  			`Backend returned code ${error.status}, ` +
  			`body was: ${error.error}`);
  	}
  	return throwError('Something bad happened; please try again later.');
  }

  private extractData(res: Response) {
  	let body = res;
  	return body || { };
  }

  getDataUser(): Observable<any> {
  	// return this.http.get(apiUrl, httpOptions).pipe(
  	return this.http.get(apiUrl+'/users').pipe(
  		map(this.extractData),
  		catchError(this.handleError));
  }

  getUserDetails(id): Observable<any> {
  	// return this.http.get(apiUrl, httpOptions).pipe(
  	return this.http.get(apiUrl+'/users/'+id).pipe(
  		map(this.extractData),
  		catchError(this.handleError));
  }

  getPostData(): Observable<any> {
  	// return this.http.get(apiUrl, httpOptions).pipe(
  	return this.http.get(apiUrl+'/posts').pipe(
  		map(this.extractData),
  		catchError(this.handleError));
  }

  getPostList(): Observable<any> {
    return this.http
      .get(`${this.URL}posts`)
      .pipe(
        (map(response => response)),
        catchError((error: any) => of(error))
      );
  }

  

  getDefaultLanguage(): Promise <any> {
  	return new Promise( (resolve, reject) => {
  		this.http
      .get(`${this.URL}posts`).then((res) => {
      	resolve(res);
  		});
  	});
  }


  getAlbumsData(): Observable<any> {
  	let response = this.http.get(`${this.URL}albums`)
  	.pipe(
        (map(response => response)),
        catchError((error: any) => of(error))
      );
  	return response;
  }

  TEST_API(){

  	return new Promise((resolve, reject) => {

  		// let user = {pseudo:'francouille',password:'Tozoo$123',confirmPassword:'Tozoo$123',email:'zango@tozoo.fr'};
  		// let data: URLSearchParams = this.serialize(user);
  		let url = `${this.URL}albums`;

  		let http = new XMLHttpRequest();

  		http.open("GET", url, true);

  		http.onload = () => resolve(JSON.parse(http.responseText));
  		http.onerror = () => reject(http.statusText);

  		http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  		http.send(data);
  	});
  }


  getPosts1() {
    const promise = new Promise((resolve, reject) => {
      // const apiURL = this.api;
      const apiURL = `${this.URL}albums`;
      this.http
        .get<any>(apiURL)
        .toPromise()
        .then((res: any) => {
        	console.log(res);
          // Success
          /*console.log(res);
          this.data = res.map((res: any) => {
            // return new Post(
            //   res.userId,
            //   res.id,
            //   res.title,
            //   res.body
            // );
          });
          resolve();*/
        },
          err => {
            // Error
            // reject(err);
          }
        );
    });
    return promise;
  }
  getPosts() {
    const promise = new Promise((resolve, reject) => {
      // const apiURL = this.api;
      const apiURL = `${this.URL}albums`;
      this.http
        .get<any>(apiURL)
        .toPromise()
        .then((res: any) => {
          // Success
          /*this.data = res.map((res: any) => {
            return new Post(
              res.userId,
              res.id,
              res.title,
              res.body
            );
          });*/
          return res;
          resolve();
        },
          err => {
            // Error
            reject(err);
            return err;
          }
        );
    });
    return promise;
  }

}
