import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable()
export class ApiService {

  public URL = 'https://jsonplaceholder.typicode.com/';

  constructor(public http: HttpClient) {
    console.log('Hello ApiProvider Provider');
  }

  getPostList(): Observable<any> {
    console.log('Sign In User: ');
    return this.http
      .get(`${this.URL}posts`)
      .pipe(
        (map(response => response)),
        catchError((error: any) => of(error))
      );
  }

}
