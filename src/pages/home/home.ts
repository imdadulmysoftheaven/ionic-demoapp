import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { ApiProvider } from '../../providers/api/api';
import { Observable } from 'rxjs/Observable';

import { UserDetailsPage } from '../user-details/user-details'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	data: string;
	// postData: any;
	postData: Observable<any>;
	datauser: any;

  constructor(public navCtrl: NavController, 
  			  private http: HttpClient,
  			  private loadingCtrl: LoadingController, 
  			  private apiProvider: ApiProvider) {
	// this.data = '';
	this.getDataUser();
  }

  ionViewDidEnter(){
  	// this.getDataUser();
  }

  async getDataUser() {
  	let loading = this.loadingCtrl.create();
	loading.present();
    await this.apiProvider.getDataUser()
      .subscribe(res => {
        console.log(res);
        loading.dismiss();
        this.datauser = res;
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

  itemDetails(id){
  	console.log(id);
  	this.navCtrl.push(UserDetailsPage, {userId: id});
  	// (page2,{item:item});
  }

}
