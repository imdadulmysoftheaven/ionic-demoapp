import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { ApiProvider } from '../../providers/api/api';
import { Observable } from 'rxjs/Observable';
/**
 * Generated class for the UserDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-details',
  templateUrl: 'user-details.html',
})
export class UserDetailsPage {
  userId: any;
  userDetails: any;
  user =  {
                name: '',
                email: '',
                phone: '',
                address: '',
                website: '',
               	company: ''        
            };

  constructor(public navCtrl: NavController,
  			  private http: HttpClient, 
  			  private loadingCtrl: LoadingController,
  			  private apiProvider: ApiProvider,
  			  public navParams: NavParams) 
  {
  	this.userId = this.navParams.get('userId');
  	this.getUserDetails();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserDetailsPage');
  }

  async getUserDetails() {
  	let loading = this.loadingCtrl.create();
	loading.present();
    await this.apiProvider.getUserDetails(this.userId)
      .subscribe(res => {
        console.log(res);
        this.userDetails = res;

        this.user.name = res.name;
        this.user.email = res.email;
        this.user.phone = res.phone;
        this.user.website = res.website;
        this.user.address = res.address.street + ", " + res.address.city;
        this.user.company = res.company.name;

        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

}
